Role Name
=========

A role that installs the eXist-db service, <http://exist-db.org/exist/apps/homepage/index.html>

Role Variables
--------------

The most important variables are listed below:

``` yaml
exist_db_major: 5
exist_db_minor: 2
exist_db_patch: 0
exist_db_http_port: 8080

exist_db_home: '/srv/existdb'
exist_db_data_dir: '{{ exist_db_home }}/data'
exist_db_journal_dir: '{{ exist_db_home }}/data-journal'
exist_db_backup_dir: '{{ exist_db_home }}/data-backups'
exist_db_bin_backup_dir: '{{ exist_db_home }}/data/backup'
exist_db_base_dir: '{{ exist_db_home }}/distribution/{{ exist_db_distribution_dir }}'
exist_db_logdir: '/var/log/exist-db'
#exist_db_conf_dir: '/srv/existdb/etc'
exist_db_conf_dir: '{{ exist_db_home }}/distribution/{{ exist_db_distribution_dir }}/etc'

# Always express it in 'm' (MegaBytes)
exist_db_min_java_heap: '512'
exist_db_max_java_heap: '{{ exist_db_min_java_heap }}'
# exist_db_max_java_heap / 3
exist_db_cache_size: '170'
exist_db_file_encoding: 'UTF-8'
exist_db_java_opts: "-Xms{{ exist_db_min_java_heap }}m -Xmx{{ exist_db_max_java_heap }}m -server -Djava.awt.headless=true  -Dfile.encoding={{ exist_db_file_encoding }}"

exist_db_consistency_enabled: True
exist_db_check_cron: "0 0 0/3 * * ?"
exist_db_max_backups_enabled: 6
exist_db_backups_enabled: True
exist_db_incremental_backups_enabled: "yes"
exist_db_backup_cron: "0 0 0/12 * * ?"

# Only works with deb based distributions and when our basic-system-setup role is used by the playbook
exist_db_tmpreaper_install: True
exist_db_tmpreaper_dirs: '{{ exist_db_bin_backup_dir }}/. {{ exist_db_backup_dir }}/.'
exist_db_tmpreaper_time: '10d'

# Setup nginx in front of the service
exist_db_nginx_setup: True
exist_db_nginx_use_common_virthost: True
exist_db_nginx_define_x_real_ip: True
exist_db_nginx_set_original_uri: True
exist_db_nginx_virtualhost_name: '{{ ansible_fqdn }}'
exist_db_nginx_server_name: '{{ exist_db_nginx_virtualhost_name }}'
exist_db_nginx_server_aliases: ''
nginx_virthosts: '{{ exist_db_nginx_virthosts }}'
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
